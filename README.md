UUID Generation Strategy Test
=============================

1) install GraphiQL app at https://electronjs.org/apps/graphiql
2) run the project by right clicking KotspringApplication.kt and run.
3) use http://localhost:8080/graphql in GraphiQL.
4) Author is the model that uses @GeneratedValue(generator = "uuid2") so test by using createAuthor in GraphiQL.
Ex: mutation{
      createAuthor(name: "Author 1"){
        id
        name
      }
    }
Queries for books are authors, countAuthors.
5) Book is the model that uses @GeneratedValue(strategy = GenerationType.AUTO) so test by using createBook in GraphiQL
Ex: mutation{
      createBook(name: "Book 1",content:"abc"){
        id
        name
        content
      }
    }
Queries for books are books and countBooks.