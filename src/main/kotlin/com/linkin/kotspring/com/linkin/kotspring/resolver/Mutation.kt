package com.linkin.kotspring.com.linkin.kotspring.resolver

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import com.linkin.kotspring.com.linkin.kotspring.entity.Author
import com.linkin.kotspring.com.linkin.kotspring.entity.Book
import com.linkin.kotspring.com.linkin.kotspring.repository.AuthorRepository
import com.linkin.kotspring.com.linkin.kotspring.repository.BookRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import java.util.*
import kotlin.collections.HashSet

@Transactional
@Component
class Mutation : GraphQLMutationResolver {

    @Autowired
    lateinit var bookRepository: BookRepository

    @Autowired
    lateinit var authorRepository: AuthorRepository

    fun createBook(name: String, content: String): Book? {
        val book = Book(name = name, content = content)
        bookRepository.save(book)
        return book
    }

    fun updateBook(id: String, name: String, content: String): Book? {
        var book = bookRepository.getOne(id)
        book.name = name
        book.content = content
        bookRepository.save(book)
        return book
    }

    fun deleteBook(id: String): Boolean {
        bookRepository.delete(bookRepository.getOne(id))
        return true
    }

    fun createAuthor(name: String): Author? {
        val author = Author(name = name)
        authorRepository.save(author)
        return author
    }

    fun updateAuthor(id: UUID, name: String): Author? {
        var author = authorRepository.getOne(id)
        author.name = name
        authorRepository.save(author)
        return author
    }

    fun deleteAuthor(id: UUID): Boolean {
        authorRepository.delete(Author(id = id))
        return true
    }
}