package com.linkin.kotspring.com.linkin.kotspring.repository

import com.linkin.kotspring.com.linkin.kotspring.entity.Book
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Transactional
@Repository
interface BookRepository : JpaRepository<Book, String>, JpaSpecificationExecutor<Book>