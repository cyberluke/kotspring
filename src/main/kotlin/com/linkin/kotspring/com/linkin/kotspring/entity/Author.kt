package com.linkin.kotspring.com.linkin.kotspring.entity

import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.LazyCollection
import org.hibernate.annotations.LazyCollectionOption
import java.util.*
import javax.persistence.*
import kotlin.collections.HashSet

@Entity
@Table(name = "author")
data class Author(
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", columnDefinition = "VARCHAR(255)")
    var id: UUID? = null,

    var name: String? = null
)