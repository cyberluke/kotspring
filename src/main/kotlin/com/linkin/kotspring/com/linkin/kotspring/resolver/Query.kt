package com.linkin.kotspring.com.linkin.kotspring.resolver

import com.coxautodev.graphql.tools.GraphQLQueryResolver
import com.linkin.kotspring.com.linkin.kotspring.entity.Author
import com.linkin.kotspring.com.linkin.kotspring.entity.Book
import com.linkin.kotspring.com.linkin.kotspring.repository.AuthorRepository
import com.linkin.kotspring.com.linkin.kotspring.repository.BookRepository
import com.linkin.kotspring.com.linkin.kotspring.specification.AuthorSpecification
import com.linkin.kotspring.com.linkin.kotspring.specification.BookSpecification
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Transactional
@Component
class Query : GraphQLQueryResolver {

    @Autowired
    lateinit var bookRepository: BookRepository

    @Autowired
    lateinit var authorRepository: AuthorRepository

    fun book(id: String?) = bookRepository.findByIdOrNull(id)

    fun books(id: String?, name: String?, content: String?): MutableList<Book> {
        val example = Book(id = id, name = name, content = content)
        return bookRepository.findAll(BookSpecification(example = example))
    }

    fun countBooks(id: String?, name: String?, content: String?): Long {
        val example = Book(id = id, name = name, content = content)
        return bookRepository.count(BookSpecification(example = example))
    }

    fun author(id: UUID?) = authorRepository.findByIdOrNull(id)

    fun authors(id: UUID?, name: String?): MutableList<Author> {
        val example = Author(id = id, name = name)
        return authorRepository.findAll(AuthorSpecification(example = example))
    }

    fun countAuthors(id: UUID?, name: String?): Long {
        val example = Author(id = id, name = name)
        return authorRepository.count(AuthorSpecification(example = example))
    }
}