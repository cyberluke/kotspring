package com.linkin.kotspring.com.linkin.kotspring.specification

import com.linkin.kotspring.com.linkin.kotspring.entity.Book
import org.springframework.data.jpa.domain.Specification
import javax.persistence.criteria.*

class BookSpecification(var example: Book) : Specification<Book> {

    override fun toPredicate(root: Root<Book>, cq: CriteriaQuery<*>, cb: CriteriaBuilder): Predicate? {
        val predicates: MutableList<Predicate> = ArrayList()
        if (example.id != null) {
            predicates.add(cb.equal(cb.toLong(root.get("id")), example.id))
        }
        if (example.name != null) {
            predicates.add(cb.like(cb.lower(root.get("name")), "%" + example.name?.toLowerCase() + "%"))
        }
        if (example.content != null) {
            predicates.add(cb.like(cb.lower(root.get("content")), "%" + example.content?.toLowerCase() + "%"))
        }
        return cb.and(*predicates.toTypedArray())
    }
}