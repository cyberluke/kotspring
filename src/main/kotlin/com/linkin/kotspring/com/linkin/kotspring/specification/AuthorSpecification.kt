package com.linkin.kotspring.com.linkin.kotspring.specification

import com.linkin.kotspring.com.linkin.kotspring.entity.Author
import org.springframework.data.jpa.domain.Specification
import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Predicate
import javax.persistence.criteria.Root

class AuthorSpecification(var example: Author) : Specification<Author> {

    override fun toPredicate(root: Root<Author>, cq: CriteriaQuery<*>, cb: CriteriaBuilder): Predicate? {
        val predicates: MutableList<Predicate> = ArrayList()
        if (example.id != null) {
            predicates.add(cb.equal(cb.toLong(root.get("id")), example.id))
        }
        if (example.name != null) {
            predicates.add(cb.like(cb.lower(root.get("name")), "%" + example.name?.toLowerCase() + "%"))
        }
        return cb.and(*predicates.toTypedArray())
    }

}