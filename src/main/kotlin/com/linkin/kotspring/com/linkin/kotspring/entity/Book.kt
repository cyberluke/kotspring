package com.linkin.kotspring.com.linkin.kotspring.entity

import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.LazyCollection
import org.hibernate.annotations.LazyCollectionOption
import java.util.*
import javax.persistence.*
import kotlin.collections.HashSet

@Entity
@Table(name = "book")
data class Book(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    var id: String? = null,

    var name: String? = null,

    var content: String? = null

)