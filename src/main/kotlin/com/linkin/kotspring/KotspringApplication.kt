package com.linkin.kotspring

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.transaction.annotation.Transactional

@SpringBootApplication
@Transactional
class KotspringApplication

fun main(args: Array<String>) {
    runApplication<KotspringApplication>(*args)
}
